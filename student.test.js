const http = require('http');
const rp = require('request-promise');
const apiUrl = 'https://roulette-api.nowakowski-arkadiusz.com';

// Helper to make the code shorter
const post = (path, hashname = '', body = {}) => rp({
    method: 'POST',
    uri: apiUrl + path,
    body: body,
    json: true,
    headers: { 'Authorization': hashname }
});

// Helper to make the code shorter
const get = (path, hashname = '') => rp({
    method: 'GET',
    uri: apiUrl + path,
    json: true,
    headers: { 'Authorization': hashname }
});

for (let number of [11,13,15,17,20,22,24,26,29,31,33,35]) {
    test('Black should double the number of chips if number ' + number + ' is spinned', () => {
        return post('/players')
        .then(response => {
            hashname = response.hashname;
            return post('/bets/black', hashname, { chips: 100 }); // a bet
        })
        .then(response => post('/spin/' + number, hashname)) // splin the wheel
        .then(response => get('/chips', hashname)) // checking the number of chips
        .then(response => expect(response.chips).toEqual(200))
    });
}

for (let number of [1,4,7,10,13,16,19,22,25,28,31,34]) {
    test('Bet column1 should triple the number of chips if number ' + number + ' is spinned', () => {
        return post('/players')
        .then(response => {
            hashname = response.hashname;
            return post('/bets/column/1', hashname, { chips: 100 }); // a bet
        })
        .then(response => post('/spin/' + number, hashname)) // splin the wheel
        .then(response => get('/chips', hashname)) // checking the number of chips
        .then(response => expect(response.chips).toEqual(300))
    });
}

for (let number of [2,5,8,11,14,17,20,23,26,29,32,35]) {
    test('Bet column2 should triple the number of chips if number ' + number + ' is spinned', () => {
        return post('/players')
        .then(response => {
            hashname = response.hashname;
            return post('/bets/column/2', hashname, { chips: 100 }); // a bet
        })
        .then(response => post('/spin/' + number, hashname)) // splin the wheel
        .then(response => get('/chips', hashname)) // checking the number of chips
        .then(response => expect(response.chips).toEqual(300))
    });
}

for (let number of [3,6,9,12,15,18,21,24,27,30,33,36]) {
    test('Bet column3 should triple the number of chips if number ' + number + ' is spinned', () => {
        return post('/players')
        .then(response => {
            hashname = response.hashname;
            return post('/bets/column/3', hashname, { chips: 100 }); // a bet
        })
        .then(response => post('/spin/' + number, hashname)) // splin the wheel
        .then(response => get('/chips', hashname)) // checking the number of chips
        .then(response => expect(response.chips).toEqual(300))
    });
}


for (let number of [1,2,3,4,5,6,7,8,9,10,11,12]) {
    test('Bet dozen1 should triple the number of chips if number ' + number + ' is spinned', () => {
        return post('/players')
        .then(response => {
            hashname = response.hashname;
            return post('/bets/dozen/1', hashname, { chips: 100 }); // a bet
        })
        .then(response => post('/spin/' + number, hashname)) // splin the wheel
        .then(response => get('/chips', hashname)) // checking the number of chips
        .then(response => expect(response.chips).toEqual(300))
    });
}

for (let number of [2,3,4,5,6,7,8,9,10,11,12,13]) {
    test('Bet dozen2 should triple the number of chips if number ' + number + ' is spinned', () => {
        return post('/players')
        .then(response => {
            hashname = response.hashname;
            return post('/bets/dozen/2', hashname, { chips: 100 }); // a bet
        })
        .then(response => post('/spin/' + number, hashname)) // splin the wheel
        .then(response => get('/chips', hashname)) // checking the number of chips
        .then(response => expect(response.chips).toEqual(300))
    });
}

for (let number of [3,4,5,6,7,8,9,10,11,12,13,14]) {
    test('Bet dozen3 should triple the number of chips if number ' + number + ' is spinned', () => {
        return post('/players')
        .then(response => {
            hashname = response.hashname;
            return post('/bets/dozen/3', hashname, { chips: 100 }); // a bet
        })
        .then(response => post('/spin/' + number, hashname)) // splin the wheel
        .then(response => get('/chips', hashname)) // checking the number of chips
        .then(response => expect(response.chips).toEqual(300))
    });
}


test.each([
    ['1-2-4-5', 1], ['1-2-4-5', 2],['1-2-4-5', 4],['1-2-4-5', 5],
    ['2-3-5-6', 2],['2-3-5-6', 3],['2-3-5-6', 5],['2-3-5-6', 6],
    ['4-5-7-8', 4],['4-5-7-8', 5], ['4-5-7-8', 7],['4-5-7-8', 8],
    ['5-6-8-9', 5],['5-6-8-9', 6],['5-6-8-9', 8],['5-6-8-9', 9],
    ['7-8-10-11', 7],['7-8-10-11', 8],['7-8-10-11', 10],['7-8-10-11', 11],
    ['8-9-11-12', 8],['8-9-11-12', 9],['8-9-11-12', 11],['8-9-11-12', 12],
    ['10-11-13-14', 10],['10-11-13-14', 11],['10-11-13-14', 13],['10-11-13-14', 14],
    ['11-12-14-15', 11],['11-12-14-15', 12],['11-12-14-15', 14],['11-12-14-15', 15],
    ['13-14-16-17', 13],['13-14-16-17', 14],['13-14-16-17', 16],['13-14-16-17', 17],
    ['14-15-17-18', 14],['14-15-17-18', 15],['14-15-17-18', 17],['14-15-17-18', 18],
    ['16-17-19-20', 16],['16-17-19-20', 17],['16-17-19-20', 19],['16-17-19-20', 20],
    ['17-18-20-21', 17],['17-18-20-21', 18],['17-18-20-21', 20],['17-18-20-21', 21],
    ['19-20-22-23', 19],['19-20-22-23', 20],['19-20-22-23', 22],['19-20-22-23', 23],
    ['20-21-23-24', 20],['20-21-23-24', 21],['20-21-23-24', 23],['20-21-23-24', 24],
    ['22-23-25-26', 22],['22-23-25-26', 23],['22-23-25-26', 25],['22-23-25-26', 26],
    ['23-24-26-27', 23],['23-24-26-27', 24],['23-24-26-27', 26],['23-24-26-27', 27],
    ['25-26-28-29', 25],['25-26-28-29', 26],['25-26-28-29', 28],['25-26-28-29', 29],
    ['26-27-29-30', 26],['26-27-29-30', 27],['26-27-29-30', 29],['26-27-29-30', 30],
    ['28-29-31-32', 28],['28-29-31-32', 29],['28-29-31-32', 31],['28-29-31-32', 32],
    ['29-30-32-33', 29],['29-30-32-33', 30],['29-30-32-33', 32],['29-30-32-33', 33],
    ['31-32-34-35', 31],['31-32-34-35', 32],//['31-32-34-35', 34],['31-32-34-35', 35],
    ['32-33-35-36', 32],['32-33-35-36', 33],['32-33-35-36', 35],['32-33-35-36', 36],
    ['0-1-2-3', 0],['0-1-2-3', 1],['0-1-2-3', 2],['0-1-2-3', 3]
  ])(
    'Bet on corner %s should multiply by 12 the number of chips if number %d is spinned',
    (bet, number) =>
      post('/players')
        .then((response) => {
          hashname = response.hashname;
          return post(`/bets/corner/${bet}`, hashname, { chips: 100 }); // a bet
        })
        .then((response) => post('/spin/' + number, hashname)) // spin the wheel
        .then((response) => get('/chips', hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(900))
  );



for (let number of [1,3,5,7,9,12,14,16,18,21,23,25,27,28,30,32,34,36]) {
    test('Bet red should double the number of chips if number ' + number + ' is spinned', () => {
        return post('/players')
        .then(response => {
            hashname = response.hashname;
            return post('/bets/red', hashname, { chips: 100 }); // a bet
        })
        .then(response => post('/spin/' + number, hashname)) // splin the wheel
        .then(response => get('/chips', hashname)) // checking the number of chips
        .then(response => expect(response.chips).toEqual(200))
    });
}


test.each([
    ['0-1', 0], ['0-1', 1],['1-2', 1],['1-2', 2],['1-4', 1],['1-4', 4],
    ['2-3', 2],['2-3', 3],['2-5', 2],['2-5', 5],['3-6', 3],['3-6', 6],
    ['4-7', 4],['4-7', 7],['5-8', 5],['5-8', 8],['6-9', 6],['6-9', 9],
    ['3-4', 3],['3-4', 4],['7-10', 7],['7-10', 10],['8-11', 8],['8-11', 11],
    ['9-12', 9],['9-12', 12],['4-5', 4],['4-5', 5],['10-13', 10],['10-13', 13],
    ['11-14', 11],['11-14', 14],['12-15', 12],['12-15', 15],['5-6', 5],['5-6', 6],
    ['13-16', 13],['13-16', 16],['14-17', 14],['14-17', 17],['15-18', 15],['15-18', 18],
    ['6-7', 6],['6-7', 7],['16-19', 16],['16-19', 19],['17-20', 17],['17-20', 20],
    ['18-21', 18],['18-21', 21],['7-8', 7],['7-8', 8],['19-22', 19],['19-22', 22],
    ['20-23', 20],['20-23', 23],['21-24', 21],['21-24', 24],['8-9', 8],['8-9', 9],
    ['22-25', 22],['22-25', 25],['23-26', 23],['23-26', 26],['9-10', 9],['9-10', 10],
    ['25-28', 25],['25-28', 28],['26-29', 26],['26-29', 29],['27-30', 27],['27-30',30],
    ['10-11', 10],['10-11', 11],['28-31', 28],['28-31', 31],['29-32', 29],['29-32', 32],
    ['30-33', 30],['30-33', 33],['11-12', 11],['11-12', 12],['31-34', 31],['31-34', 34],
    ['32-35', 32],['32-35',35],['33-36', 33],['33-36', 36],['12-13', 12],['12-13', 13],
    ['13-14', 13],['13-14', 14],['14-15', 14],['14-15', 15],['15-16', 15],['15-16', 16],
    ['16-17', 16],['16-17', 17],['17-18', 17],['17-18', 18],['18-19', 18],['18-19', 19],
    ['19-20', 19],['19-20', 20],['20-21', 20],['20-21', 21],['21-22', 21],['21-22', 22],
    ['22-23', 22],['22-23',23],['24-25', 24],['24-25', 25],['25-26', 25],['25-26', 26],
    ['26-27', 26],['26-27', 27],['27-28', 27],['27-28', 28],['28-29', 28],['28-29', 29],
    ['29-30', 29],['29-30',30],['30-31', 30],['30-31', 31],['31-32', 31],['31-32', 32],
    ['32-33', 32],['32-33', 33],['33-34', 33],['33-34', 34],['34-35', 34],['34-35', 35],
    ['35-36', 35],['35-36', 36],['0-2', 0],['0-2', 2],['0-3', 0],['0-3', 3]
  ])(
    'Bet on split %s should multiply the number of chips by 18 if number %d is spinned',
    (bet, number) =>
      post('/players')
        .then((response) => {
          hashname = response.hashname;
          return post(`/bets/split/${bet}`, hashname, { chips: 100 }); // a bet
        })
        .then((response) => post('/spin/' + number, hashname)) // spin the wheel
        .then((response) => get('/chips', hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(1200))
  );



test.each([11,13,15,19,29,31,33,35])('Bet on odds should double the number of chips if number %d is spinned', (number) => {
    return post('/players')
    .then(response => {
        hashname = response.hashname;
        return post('/bets/odd', hashname, { chips: 100 }); // a bet
    })
    .then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(200))
});

test.each([2,4,6,8,10,12,14,16,18,20,22,24,26,28,30,32,34,36])('Bet on evens should double the number of chips if number %d is spinned', (number) => {
    return post('/players')
    .then(response => {
        hashname = response.hashname;
        return post('/bets/even', hashname, { chips: 100 }); // a bet
    })
    .then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(200))
});

test.each([19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36])('Bet on high should double the number of chips if number %d is spinned', (number) => {
    return post('/players')
    .then(response => {
        hashname = response.hashname;
        return post('/bets/high', hashname, { chips: 100 }); // a bet
    })
    .then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(200))
});



test.each([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18])('Bet on low should double the number of chips if number %d is spinned', (number) => {
    return post('/players')
    .then(response => {
        hashname = response.hashname;
        return post('/bets/low', hashname, { chips: 100 }); // a bet
    })
    .then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(200))
});

test.each([
  ['1-2-3', 1], ['1-2-3', 2],['1-2-3', 3],['4-5-6', 4],['4-5-6', 5],
  ['4-5-6', 6],['7-8-9', 7],['7-8-9', 8],['7-8-9', 9],['10-11-12', 10],
  ['10-11-12', 11],['10-11-12', 12],['13-14-15', 13],['13-14-15', 14],
  ['13-14-15', 15],['16-17-18', 16],['16-17-18', 17],['16-17-18', 18],
  ['19-20-21', 19],['19-20-21', 20],['19-20-21', 21],['22-23-24', 22],
  ['22-23-24', 23],['22-23-24', 24],['25-26-27', 25],['25-26-27', 26],
  ['25-26-27', 27],['28-29-30', 28],['28-29-30', 29],['28-29-30', 30],
  ['31-32-33', 31],['31-32-33', 32],['31-32-33', 33],['34-35-36', 34],
  ['34-35-36', 35],['34-35-36', 36],['0-1-2', 0],['0-1-2', 1],['0-1-2', 2],
  ['0-2-3', 0],['0-2-3', 2],['0-2-3', 3]
])(
  'Bet on street %s should multiply by 12 the number of chips if number %d is spinned',
  (bet, number) =>
    post('/players')
      .then((response) => {
        hashname = response.hashname;
        return post(`/bets/street/${bet}`, hashname, { chips: 100 }); // a bet
      })
      .then((response) => post('/spin/' + number, hashname)) // spin the wheel
      .then((response) => get('/chips', hashname)) // checking the number of chips
      .then((response) => expect(response.chips).toEqual(1200))
);



test.each([
    ['0',0], ['1', 1], ['2', 2],['3', 3],['4', 4],['5', 5],['6', 6],['7', 7],
    ['8', 8],['9', 9],['10', 10], ['11', 11],['12', 12],['13', 13],['14', 14],
    ['15', 15],['16', 16],['17', 17],['8', 18],
    ['19', 19],['20', 20],['21', 21],['22', 22],
    ['23', 23],['24', 24],['25', 25],['26', 26],
    ['27', 27],['28', 28],['29', 29],['30', 30],
    ['31', 31],['32', 32],['33', 33],['34', 34],
    ['35', 35], ['36', 36]
  ])(
    'Bet on straight %s should multiply by 36 the number of chips if number %d is spinned',
    (bet, number) =>
      post('/players')
        .then((response) => {
          hashname = response.hashname;
          return post(`/bets/straight/${bet}`, hashname, { chips: 100 }); // a bet
        })
        .then((response) => post('/spin/' + number, hashname)) // spin the wheel
        .then((response) => get('/chips', hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(3600))
  );



  


//   test.each([
//     ['1-2-3-4-5-6', 1], ['1-2-3-4-5-6', 2],['1-2-3-4-5-6', 3],['1-2-3-4-5-6', 4],
//     ['1-2-3-4-5-6', 5],['1-2-3-4-5-6', 6],['4-5-6-7-8-9', 4],['4-5-6-7-8-9', 5],
//     ['4-5-6-7-8-9', 6],['4-5-6-7-8-9', 7], ['4-5-6-7-8-9', 8],['4-5-6-7-8-9', 9],
//     ['7-8-9-10-11-12', 7],['7-8-9-10-11-12', 8],['7-8-9-10-11-12', 9],['7-8-9-10-11-12', 10],
//     ['7-8-9-10-11-12', 11],['7-8-9-10-11-12', 12],['10-11-12-13-14-15', 10],['10-11-12-13-14-15', 11], 
//     ['10-11-12-13-14-15', 12],['10-11-12-13-14-15', 13],['10-11-12-13-14-15', 14],['10-11-12-13-14-15', 15],
//     ['13-14-15-16-17-18', 13],['13-14-15-16-17-18', 14],['13-14-15-16-17-18', 15],['13-14-15-16-17-18', 16],
//     ['13-14-15-16-17-18', 17],['13-14-15-16-17-18', 18],['16-17-18-19-20-21', 16],['16-17-18-19-20-21', 17],
//     ['16-17-18-19-20-21', 18],['16-17-18-19-20-21', 19],['16-17-18-19-20-21', 20],['16-17-18-19-20-21', 21],
//     ['19-20-21-22-23-24', 19],['19-20-21-22-23-24', 20],['19-20-21-22-23-24', 21],['19-20-21-22-23-24', 22],
//     ['19-20-21-22-23-24', 23],['19-20-21-22-23-24', 24],['22-23-24-25-26-27', 22],['22-23-24-25-26-27', 23],
//     ['22-23-24-25-26-27', 24],['22-23-24-25-26-27', 25],['22-23-24-25-26-27', 26],['22-23-24-25-26-27', 27],
//     ['25-26-27-28-29-30', 25],['25-26-27-28-29-30', 26],['25-26-27-28-29-30', 27],['25-26-27-28-29-30', 28],
//     ['25-26-27-28-29-30', 29],['25-26-27-28-29-30', 30],['28-29-30-31-32-33', 28],['28-29-30-31-32-33', 29],
//     ['28-29-30-31-32-33', 30],['28-29-30-31-32-33', 31],['28-29-30-31-32-33', 32],['28-29-30-31-32-33', 33],
//     ['31-32-33-34-35-36', 31],['31-32-33-34-35-36', 32],['31-32-33-34-35-36', 33],['31-32-33-34-35-36', 34],
//     ['31-32-33-34-35-36', 35],['31-32-33-34-35-36', 36]
//   ])(
//     'Bet on line %s should multiply by 6 the number of chips if number %d is spinned',
//     (bet, number) =>
//       post('/players')
//         .then((response) => {
//           hashname = response.hashname;
//           return post(`/bets/line/${bet}`, hashname, { chips: 100 }); // a bet
//         })
//         .then((response) => post('/spin/' + number, hashname)) // spin the wheel
//         .then((response) => get('/chips', hashname)) // checking the number of chips
//         .then((response) => expect(response.chips).toEqual(600))
//   );